var SRC_DIR = './src/',
    BUILD_DIR = './build/';

var gulp = require('gulp');
var concat = require('gulp-concat');
var handlebars = require('gulp-ember-handlebars');

gulp.task('templates', function () {
  gulp
    .src(SRC_DIR + 'app/**/*.handlebars')
    .pipe(handlebars({
      outputType: 'browser'
    }))
    .pipe(concat('templates.js'))
    .pipe(gulp.dest(BUILD_DIR + 'public/js/'));
});

gulp.task('app', function () {
  gulp
    .src(SRC_DIR + 'app/*.js')
    .pipe(concat('app.js'))
    .pipe(gulp.dest(BUILD_DIR + 'public/js/'));
});

gulp.task('server', function () {
  gulp
    .src(SRC_DIR + 'server.js')
    .pipe(gulp.dest(BUILD_DIR));
  gulp
    .src(SRC_DIR + 'index.html')
    .pipe(gulp.dest(BUILD_DIR + 'public/'));
  gulp
    .src(SRC_DIR + 'libs/*.js')
    .pipe(gulp.dest(BUILD_DIR + 'public/js/'));
});

gulp.task('styles', function () {
  gulp
    .src(SRC_DIR + 'styles/**/*.css')
    .pipe(concat('styles.css'))
    .pipe(gulp.dest(BUILD_DIR + 'public/'));
});

gulp.task('images', function () {
  gulp
    .src(SRC_DIR + 'images/*')
    .pipe(gulp.dest(BUILD_DIR + 'public/images/'))
});

gulp.task('default', [
  'templates',
  'server',
  'app',
  'styles',
  'images'
]);
