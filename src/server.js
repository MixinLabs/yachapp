var http = require('http'),
    sockjs = require('sockjs'),
    express = require('express'),
    app = express();

var clients = {},
    messages = [];

var server = http.createServer(app),
    chat = sockjs.createServer({
      sockjs_url: "http://cdn.sockjs.org/sockjs-0.3.4.min.js"
    });

function send (conn, data) {
  return conn.write(JSON.stringify(data));
}

function broadcast (msg) {
  var data = JSON.stringify(msg);
  Object.keys(clients)
        .forEach(function (clientId) {
          send(clients[clientId], msg);
        });
}

chat.on('connection', function (conn) {
  clients[conn.id] = conn;

  send(conn, { type: 'init', messages: messages });

  conn.on('data', function (msg) {
    var timestamp = (new Date()).getTime(),
        data = { type: 'msg', message: msg, timestamp: timestamp };

    messages.push(data);
    broadcast(data);
  });
});

chat.installHandlers(server, { prefix:'/_chat' });
app.use('/', express.static(__dirname + '/public'));

console.log('Starting server on http://localhost:9000');
server.listen(process.env.PORT || 9000, '0.0.0.0');
