(function (App, Ember) {
  "use strict";

  App.ChatMessageComponent = Ember.Component.extend({
    classNames: ['chat-message', 'animated', 'slideInDown'],
    pic: function () {
      var urls = [
      'kitten_1.jpg',
      'kitten_2.jpg',
      'kitten_3.jpg'
      ];
      var picIdx = Math.floor(Math.random() * urls.length);
      return '/images/' + urls[picIdx];
    }.property()
  });

}).call(this, App, Ember);
