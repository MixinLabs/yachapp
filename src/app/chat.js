(function (App, Ember) {
  "use strict";

  App.ChatRoute = Ember.Route.extend({
    setupController: function (controller) {
      var sock = new SockJS('http://172.16.0.4:9000/_chat');

      sock.onmessage = function (e) {
        var data = JSON.parse(e.data);

        if (data.type === 'init') {
          controller.set('content', data.messages);
          return;
        }

        controller.content.pushObject(JSON.parse(e.data));
      };
      controller.set('sock', sock);
    }
  });

  App.ChatController = Ember.ArrayController.extend({
    sortProperties: ['timestamp'],
    sortAscending: false,
    content: [],
    message: '',
    sock: null,
    actions: {
      sendMessage: function (msg) {
        var sock = this.get('sock');
        sock.send(msg);
        this.set('message', '');
      }
    }
  });
}).call(this, window.App, Ember);
