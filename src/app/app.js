(function (w, Ember) {
  "use strict";

  var App = window.App = Ember.Application.create();
  App.Router.map(function () {
    this.route('chat', { path: '/' });
  });

}).call(this, window, Ember);
